require 'rails_helper'

RSpec.describe 'ArticlesController', type: :request do
  describe 'check if all GET paths work' do
    it 'renders the index template' do
      get root_path
      expect(response).to render_template('index')
    end
    it 'has 200 status' do
      get root_path
      expect(response).to have_http_status(200)
    end
    it 'created article is correct' do
      article = create(:article, title: 'testing', body: 'testing body content', status: 'public')
      article2 = create(:article, title: 'test article 2', body: 'content body 2', status: 'private')
      get root_path
      expect(response.body).to include(article.title)
      expect(response.body).to include(article2.title)
    end
    it 'get an existing article would render the show template' do
      article = create(:article, title: 'testing', body: 'testing body content', status: 'public')
      get article_path(article)
      expect(response.body).to include(article.title, article.body, article.status, 'Add a comment')
    end
    it 'renders the new article template' do
      get new_article_path
      expect(response.body).to include('New Article')
    end
    it 'opens the edit article template' do
      article = create(:article, title: 'testing', body: 'testing body content', status: 'public')
      get edit_article_path(article)
      expect(response.body).to include(article.title, 'Edit Article')
    end
  end
  describe 'article creation' do
    let(:valid_params) do
      {
        article: { title: 'testing', body: 'testing body content', status: 'public' }
      }
    end
    let(:invalid_params) do
      {
        article: { title: 'testing'}
      }
    end
    it 'check title, body and status is correct' do
      article = build(:article, title: 'testing', body: 'testing body content', status: 'public')

      expect(article.title).to eq('testing')
      expect(article.body).to eq('testing body content')
      expect(article.status).to eq('public')
    end

    it 'check if article can be created and redirect to the article page' do
      post '/articles', params: valid_params
      expect(response).to redirect_to(article_path(assigns(:article)))
    end
    it 'article should not be created when body is empty and re-render new template' do
      get new_article_path
      expect(response.body).to include('New Article')
      post '/articles', params: invalid_params
      expect(response).to render_template('new')
      expect(response).to have_http_status(200)
    end
  end
  describe 'article editing' do
    it 'edit article should redirect to the article page' do
      article = create(:article, title: 'testing', body: 'testing body content', status: 'public')
      temp = article_path(article)
      put temp.to_s, params: { article: { title: 'edited', body: 'edited body content', status: 'public' } }
      expect(response).to redirect_to(article_path(article))
      follow_redirect!
      expect(response.body).to include('edited', 'edited body content')
    end
    it 'invalid edit article should re render edit page' do
      article = create(:article, title: 'testing', body: 'testing body content', status: 'public')
      temp = article_path(article)
      put temp.to_s, params: { article: { title: 'edited', body: '', status: 'public' } }
      expect(response).to render_template('edit')
    end
  end
  describe 'article delete' do
    it 'expect to redirect to index after destroying an article' do
      article = create(:article, title: 'testing', body: 'testing body content', status: 'public')
      temp = article_path(article)
      delete temp.to_s
      expect(response).to redirect_to root_path
    end
  end
end
